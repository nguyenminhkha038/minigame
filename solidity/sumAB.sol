// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.4;

contract Sum{
    uint a;
    uint b;
    
    function setA(uint _a)external {
        a=_a;
    }
    function getA() external view returns(uint) {
        return a;
    }
     function setB(uint _a)external {
        b=_a;
    }
    function getB() external view returns(uint) {
        return b;
    }
    
    function SumAB() external view returns(uint){
        return a+b;
    }
}