pragma solidity ^0.8.4;
contract MiniGame {
    Student[] public arrStudent;
    struct Student{
        string _Id;
        address _wallet;
    }
   
    event send(address _wallet, string _id);
    event balanceEvent(uint _balance);
    function Register(string memory _id) payable public{
        require(msg.value == 1 ether);
        Student memory newStudent = Student(_id,msg.sender);
        arrStudent.push(newStudent);
        emit send(msg.sender, _id);
    }
    function balanceOf() public returns(uint){
        emit balanceEvent(address(this).balance);
        return address(this).balance;
    }
    function sendEther(address payable recipient,uint _amount) external{
        require(address(this).balance/10**18 >= _amount,"Not enough");
        recipient.transfer(_amount*10**18);
        
    }
     function sendWei(address payable recipient,uint _amount) external{
        require(address(this).balance >= _amount,"Not enough");
        recipient.transfer(_amount);
        
    }
    function sendAll(address payable recipient) external{
        require(address(this).balance >0,"Not enough");
        recipient.transfer(address(this).balance);
        
    }
}