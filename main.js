import express from "express";
import dotenv from "dotenv";
import bodyParser from "body-parser";
import path from "path";
dotenv.config();
import studentRouter from "./src/component/student/student.router.js";
import database from "./src/config/connectDB.js";
database();
const app = express();
const __dirname = path.resolve(path.dirname(""));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static("public"));
app.use(
  "/script",
  express.static(__dirname + "/node_modules/web3.js-browser/build/")
);
app.set("views", "ejs");
app.set("views", "./views");
app.use("/", studentRouter);


app.use((req, res, next) => {
  return res.status(404).json({
    error: {
      status: 404,
      message: "Page Not Found",
    },
  });
});

app.use((error, req, res, next) => {
  if (error.details) {
    if (error.details.body) {
      error.details.body.forEach((element) => {
        error.message = element.message;
      });
      return res.status(error.statusCode || 500).json({
        error: {
          status: error.statusCode || 500,
          message: error.message || "Internal Server Error",
        },
      });
    }
    if (error.details.params) {
      error.details.params.forEach((element) => {
        error.message = element.message;
      });
      return res.status(error.statusCode || 500).json({
        error: {
          status: error.statusCode || 500,
          message: error.message || "Internal Server Error",
        },
      });
    }
    if (error.details.query) {
      error.details.query.forEach((element) => {
        error.message = element.message;
      });
      return res.status(error.statusCode || 500).json({
        error: {
          status: error.statusCode || 500,
          message: error.message || "Internal Server Error",
        },
      });
    }
  }
  return res.status(error.httpCode || 500).json({
    error: {
      status: error.httpCode || 500,
      message: error.message || "Internal Server Error",
    },
  });
});
const PORT = process.env.PORT || 8088;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
export default app;
