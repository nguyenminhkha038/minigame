/*jslint es6 */
import mongoose from "mongoose";
import dotenv from "dotenv";
dotenv.config();
const URI = process.env.DATACONNECTION;
const connectDB = async () => {
  try {
     await mongoose.connect(URI, {
       useNewUrlParser: true,
       useUnifiedTopology: true,
    })
    console.log("connected...");
  } catch (error) {
    console.log(error);
  }
};

export default connectDB;