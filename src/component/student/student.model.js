import mongoose from "mongoose";
const Schema = mongoose.Schema;
const studentSchema = new Schema({
  Email: String,
  Name: String,
  Phone: String,
  Payed: {
    type: Boolean,
    default:false
  },
  Wallet: {
    type: String,
    default:null
  },
  Date: {
    type: Date,
    default:Date.now()
  }
},{
    timestamps: true,
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
  });

const student = mongoose.model("student", studentSchema);
export default student;