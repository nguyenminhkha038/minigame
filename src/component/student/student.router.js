import { Router } from "express";
import studentController from "./student.controller.js";
const studentRouter = Router();
studentRouter.post('/api/create', studentController.createStudent);
studentRouter.get('/', studentController.home);
export default studentRouter;