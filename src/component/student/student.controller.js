import student from "./student.model.js";
const createStudent = async (req, res,next) => {
  try {
    const { Email, Phone, Name, Payed, Wallet, Date } = req.body;
    const newStudent = await student.create({
      Email,
      Name,
      Phone
    })
    await newStudent.save();
    return res.json(newStudent)
  } catch(err) {
    next(err)
  }
}
const home = (req, res, next) => {
  res.render("layout.ejs")
}
export default {createStudent,home};