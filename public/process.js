$(document).ready(function () {
  checkMetaMask();
  let currentAccount;
  //bytecode smartContract
  const abi = [
    {
      anonymous: false,
      inputs: [
        {
          indexed: false,
          internalType: "uint256",
          name: "_balance",
          type: "uint256",
        },
      ],
      name: "balanceEvent",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: false,
          internalType: "address",
          name: "_wallet",
          type: "address",
        },
        {
          indexed: false,
          internalType: "string",
          name: "_id",
          type: "string",
        },
      ],
      name: "send",
      type: "event",
    },
    {
      inputs: [
        {
          internalType: "string",
          name: "_id",
          type: "string",
        },
      ],
      name: "Register",
      outputs: [],
      stateMutability: "payable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      name: "arrStudent",
      outputs: [
        {
          internalType: "string",
          name: "_Id",
          type: "string",
        },
        {
          internalType: "address",
          name: "_wallet",
          type: "address",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "balanceOf",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "address payable",
          name: "recipient",
          type: "address",
        },
      ],
      name: "sendAll",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "address payable",
          name: "recipient",
          type: "address",
        },
        {
          internalType: "uint256",
          name: "_amount",
          type: "uint256",
        },
      ],
      name: "sendEther",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "address payable",
          name: "recipient",
          type: "address",
        },
        {
          internalType: "uint256",
          name: "_amount",
          type: "uint256",
        },
      ],
      name: "sendWei",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
  ];
  //address smartContract
  const address = "0x2Fae0AEAA1048A0f2df44dC68f2dC7cB57C76989";
  const web3 = new Web3(window.ethereum);
  window.ethereum.enable();
  const contractMetaMask = new web3.eth.Contract(abi, address);

  const provider = new Web3.providers.WebsocketProvider(
    "wss://rinkeby.infura.io/ws/v3/4e94072f80f640968e95a80cff73cbd7"
  );
  const web3Infura = new Web3(provider);
  const contractInfura = web3Infura.eth.Contract(abi, address); //connect Infura
  //listening events from infura
  contractInfura.events.send(
    { filter: {}, fromBlock: "latest" },
    (err, event) => {
      if (err) {
        console.log(err);
      } else {
        $("#tblList").append(
          `<tr id="row1" style="text-align: center;">
                <td>` +
            event.returnValues[0] +
            `</td>
                <td>` +
            event.returnValues[1] +
            `</td>
              </tr>`
        );
      }
    }
  );
  $("#Register").click(() => {
    if (!currentAccount) {
      alert("Please connect MetaMask");
      return;
    }
    $.post(
      "./api/create",
      {
        Email: $("#txtEmail").val(),
        Name: $("#txtName").val(),
        Phone: $("#txtPhone").val(),
      },
      (data) => {
        contractMetaMask.methods.Register(data._id).send({
          from: currentAccount,
          value: 1000000000000000000,
        });
      }
    );
  });
  $("#connectMM").click(function () {
    connectMetaMask()
      .then((data) => {
        currentAccount = data[0];
        console.log(currentAccount);
      })
      .catch((err) => {
        console.log(err);
      });
  });
});
const checkMetaMask = () => {
  if (typeof window.ethereum !== "undefined") {
    console.log("MetaMask is installed!");
  } else {
    alert("Need install MetaMask");
  }
};

const connectMetaMask = async () => {
  const account = await ethereum.request({ method: "eth_requestAccounts" });
  return account;
};
